import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: const Text(
          '등산 동호회 소개',
        ),
        centerTitle: true,
        elevation: 0.0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.person),
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(),
              accountName: Text(
                '둘리',
              ),
              accountEmail: Text(
                'enffl01@gmail.com',
              ),
              decoration: BoxDecoration(
                color: Colors.green,
              ),
              onDetailsPressed: () {},
            ),
            ListTile(
              leading: Icon(Icons.home),
              iconColor: Colors.green,
              focusColor: Colors.green,
              title: Text('홈'),
              onTap: () {},
              trailing: Icon(Icons.navigate_next),
            ),
            ListTile(
              leading: Icon(Icons.settings),
              iconColor: Colors.green,
              focusColor: Colors.green,
              title: Text('옵션'),
              onTap: () {},
              trailing: Icon(Icons.navigate_next),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              alignment: Alignment(-1.0, -1.0),
              margin: EdgeInsets.all(10),
              child: const Text(
                '소개',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              child: Image.asset('assets/climbing.jpg'),
            ),
            Container(
              alignment: Alignment(-1.0, -1.0),
              margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
              child: const Text(
                '둘리 등산회',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              alignment: Alignment(-1.0, -1.0),
              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: const Text(
                '회비 : 50,000',
              ),
            ),
            Container(
              alignment: Alignment(-1.0, -1.0),
              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: const Text(
                '매주 토요일 새벽 6시 출발',
              ),
            ),
            Container(
              alignment: Alignment(-1.0, -1.0),
              margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
              child: const Text(
                '주요 멤버',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Row(
              children: [
                Container(
                  margin: EdgeInsets.all(3),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/sub01.png',
                        width: 130,
                        height: 120,
                        fit: BoxFit.cover,
                      ),
                      Text(
                        '회장 : 둘리',
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(3),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/sub02.png',
                        width: 130,
                        height: 120,
                        fit: BoxFit.cover,
                      ),
                      Text(
                        '부회장 : 고길동',
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(3),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/sub03.png',
                        width: 130,
                        height: 120,
                        fit: BoxFit.cover,
                      ),
                      Text(
                          '마이콜',
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Container(
                  margin: EdgeInsets.all(3),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/sub04.png',
                        width: 130,
                        height: 120,
                        fit: BoxFit.cover,
                      ),
                      Text(
                        '희동이',
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(3),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/sub05.png',
                        width: 130,
                        height: 120,
                        fit: BoxFit.cover,
                      ),
                      Text(
                        '도너',
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(3),
                  child: Column(
                    children: [
                      Image.asset(
                          'assets/sub06.png',
                        width: 130,
                        height: 120,
                        fit: BoxFit.cover,
                      ),
                      Text(
                          '또치',
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
